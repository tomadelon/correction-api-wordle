package com.zenika.academy.barbajavas.wordle.domain.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Profile({"api"})
public class ScrabbleApiDictionaryServiceTest {

    @Autowired
    ScrabbleApiDictionaryService scrabbleApiDictionaryService;

    @Test
    void getRandomWordTest() throws JsonProcessingException {
        scrabbleApiDictionaryService = Mockito.mock(ScrabbleApiDictionaryService.class);
        Mockito.when(scrabbleApiDictionaryService.getRandomWord(6)).thenReturn("tables");
        assertEquals("tables",scrabbleApiDictionaryService.getRandomWord(6) );
    }

    @Test
    public void wordExistsTest() throws JsonProcessingException, IllegalWordException {
        assertTrue(scrabbleApiDictionaryService.wordExists("Test"));
        assertFalse(scrabbleApiDictionaryService.wordExists("Teztze"));
    }
}
