package com.zenika.academy.barbajavas.configuration;

import com.zenika.academy.barbajavas.wordle.domain.service.i18n.I18n;
import com.zenika.academy.barbajavas.wordle.domain.service.i18n.I18nFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Scanner;

@Configuration
public class BeanConfig {
    @Bean
    public Scanner scanner(){return new Scanner(System.in);}

    @Bean
    public I18n getI18n(@Value("${language.value}") String value) throws Exception {
        return  I18nFactory.getI18n(value); }
}
