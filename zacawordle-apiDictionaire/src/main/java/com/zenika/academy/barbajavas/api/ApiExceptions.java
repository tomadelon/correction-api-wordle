package com.zenika.academy.barbajavas.api;

import com.zenika.academy.barbajavas.api.ApiError;
import com.zenika.academy.barbajavas.wordle.domain.service.BadLengthException;
import com.zenika.academy.barbajavas.wordle.domain.service.IllegalWordException;
import com.zenika.academy.barbajavas.wordle.domain.service.i18n.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.io.FileNotFoundException;

@ControllerAdvice
public class ApiExceptions {
    @Autowired
    I18n i18n;

    @ExceptionHandler(value ={BadLengthException.class})
    public final ResponseEntity<ApiError> lengthException() throws FileNotFoundException {
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, i18n.getMessage("nb_letters_word_try"));
        return new ResponseEntity<ApiError>(apiError, apiError.getStatus());
    }

    @ExceptionHandler(value ={IllegalWordException.class})
    public final ResponseEntity<ApiError> wordException() throws FileNotFoundException {
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, i18n.getMessage("word_not_in_dictionary"));
        return new ResponseEntity<ApiError>(apiError, apiError.getStatus());
    }

}
