package com.zenika.academy.barbajavas.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.zenika.academy.barbajavas.wordle.application.GameManager;
import com.zenika.academy.barbajavas.wordle.domain.model.Game;
import com.zenika.academy.barbajavas.wordle.domain.repository.GameRepository;
import com.zenika.academy.barbajavas.wordle.domain.service.BadLengthException;
import com.zenika.academy.barbajavas.wordle.domain.service.DictionaryService;
import com.zenika.academy.barbajavas.wordle.domain.service.IllegalWordException;
import com.zenika.academy.barbajavas.wordle.domain.service.displayer.console.color.ConsoleColorDisplayer;
import com.zenika.academy.barbajavas.wordle.domain.service.i18n.I18n;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api")
public class GameController {

    private final ConsoleColorDisplayer consoleColorDisplayer;
    private final DictionaryService dictionaryService;
    private final GameRepository gameRepository;
    private final GameManager gameManager;
    private I18n i18n;


    @Autowired
    public GameController(ConsoleColorDisplayer consoleColorDisplayer, DictionaryService DictionaryService,
                          GameRepository gameRepository, GameManager gameManager, I18n i18n) {

        this.consoleColorDisplayer = consoleColorDisplayer;
        this.dictionaryService = DictionaryService;
        this.gameRepository = gameRepository;
        this.gameManager = gameManager;
        this.i18n = i18n;
    }

    @PostMapping("/games")
    public Game createGame(@RequestParam(value = "wordLength", defaultValue = "6") Integer wordLength, @RequestParam(value = "maxAttempts", defaultValue = "6") Integer maxAttempts) throws JsonProcessingException {
        return gameManager.startNewGame(wordLength, maxAttempts);
    }

    @PostMapping("/games/{gameTid}")
    public Game guessWord(@PathVariable String gameTid, @RequestBody GuessDto guess) throws BadLengthException, IllegalWordException, JsonProcessingException {
        return gameManager.attempt(gameTid, guess.getGuess());
    }

    @GetMapping("/games/{gameTid}")
    public Optional<Game> gameState(@PathVariable String gameTid) {
         return gameRepository.findByTid(gameTid);
       // return  gameManager.getGame(gameTid).map(Game game -> ResponseEntity.ok(game)).orElse(ResponseEntity.notFound().build());
    }
}
