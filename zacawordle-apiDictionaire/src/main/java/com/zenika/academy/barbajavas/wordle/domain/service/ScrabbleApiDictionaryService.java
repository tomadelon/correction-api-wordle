package com.zenika.academy.barbajavas.wordle.domain.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
/*
to do : mettre en place la gestion de la langue,
faire le test avec le mock de getRandomWord
 */

@Profile("api")
@Component
public class ScrabbleApiDictionaryService implements DictionaryService {

    private String language;

    private ScrabbleApiDictionaryService(@Value("${language.value}") String language) {
        this.language = language.equals("FR")?"fr":"en";
    }


    @Override
    public String getRandomWord(int length) throws JsonProcessingException {
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://scrabble-api.fly.dev/api/dictionaries/"+this.language+"/randomWord?length=" + length;
        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);

        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(response.getBody());
        String mot = root.path("word").toString();
        return mot.substring(1, mot.length() - 1);
    }

    @Override
    public boolean wordExists(String word) throws JsonProcessingException {
        boolean result;
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://scrabble-api.fly.dev/api/dictionaries/"+this.language+"/words/" + word;
        try {
            ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(response.getBody());
            result = root.path("wordExists").booleanValue();
        } catch (HttpClientErrorException e) {
            result = false;
        }
        return result;
    }
}
